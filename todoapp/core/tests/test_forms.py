from django.test import TestCase

from todoapp.core.forms import TaskForm


class TaskFormTest(TestCase):
    def test_form_has_fields(self):
        form = TaskForm()
        expected = ['name']

        self.assertSequenceEqual(expected, list(form.fields))

    def test_name_is_not_optional(self):
        form = TaskForm()

        self.assertFalse(form.is_valid())
