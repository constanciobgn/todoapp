from django.test import TestCase

from todoapp.core.models import Task


class TaskTest(TestCase):
    def setUp(self):
        self.task = Task.objects.create(
            name="Task Name"
        )

    def test_should_create_a_task(self):
        self.assertEqual(1, Task.objects.count())

    def test_str(self):
        self.assertEqual("Task: Task Name", str(self.task))
