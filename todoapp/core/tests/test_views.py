from django.test import TestCase
from django.db.models.query import QuerySet

from todoapp.core.forms import TaskForm


class HomepageTest(TestCase):
    def setUp(self):
        self.resp = self.client.get('/')

    def test_should_return_ok(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_should_render_template(self):
        self.assertTemplateUsed(self.resp, 'index.html')

    def test_form_context(self):
        form = self.resp.context['form']
        self.assertIsInstance(form, TaskForm)

    def test_tasks_context(self):
        tasks = self.resp.context['tasks']
        self.assertIsInstance(tasks, QuerySet)
