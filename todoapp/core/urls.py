from django.conf.urls import url

from todoapp.core.views import homepage

urlpatterns = [
    url(r'^$', homepage, name='homepage'),
]
