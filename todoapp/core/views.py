from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib import messages

from todoapp.core.forms import TaskForm
from todoapp.core.models import Task


def homepage(request):
    tasks = Task.objects.all()

    if request.method == "POST":
        form = TaskForm(request.POST)

        if form.is_valid():
            form.save()
            messages.success(request, "Tarefa cadastrada com sucesso")
            return redirect(reverse('homepage'))
        else:
            messages.error(request, "O campo nome é obrigatório")
    else:
        form = TaskForm()
    return render(request, 'index.html', {
        'form': form,
        'tasks': tasks
    })
