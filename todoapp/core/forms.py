from django import forms

from todoapp.core.models import Task


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ('name',)
