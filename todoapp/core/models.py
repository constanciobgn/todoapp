from django.db import models


class Task(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        db_table = "tasks"

    def __str__(self):
        return f'Task: {self.name}'
