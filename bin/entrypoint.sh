#!/bin/bash

python manage.py migrate
python manage.py collectstatic --noinput

gunicorn todoapp.wsgi:application -w 2 -b :8000

