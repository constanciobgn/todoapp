from behave import given, when, then

from features.pages.todo_page import TodoPage


@given('que o usuário não cadastrar a tarefa')
def access_todolist_page(context):
    browser = context.browser
    browser.get(context.base_url)
    page = TodoPage(browser)

    assert page.title.text == "Lista de Tarefas"


@when('tentar cadastrar a tarefa sem descrição')
def click_add_button(context):
    browser = context.browser
    page = TodoPage(browser)

    page.add_button.click()


@then('deve mostrar uma mensagem de erro')
def success_message(context):
    browser = context.browser
    page = TodoPage(browser)

    assert page.alert.text == "O campo nome é obrigatório"
