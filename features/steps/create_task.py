from behave import given, when, then

from features.pages.todo_page import TodoPage


@given('que o usuário acesse a página da lista de tarefas')
def access_todolist_page(context):
    browser = context.browser
    browser.get(context.base_url)
    page = TodoPage(browser)

    assert page.title.text == "Lista de Tarefas"


@when('cadastrar a tarefa')
def insert_task_name(context):
    browser = context.browser
    page = TodoPage(browser)

    page.insert_task('Nome da Tarefa')


@then('deve mostrar a mensagem de sucesso')
def success_message(context):
    browser = context.browser
    page = TodoPage(browser)

    assert page.alert.text == "Tarefa cadastrada com sucesso"


@then('a tarefa deve ser exibida na lista de tarefas')
def todo_list(context):
    browser = context.browser
    page = TodoPage(browser)

    assert page.total_tasks() > 0
