# language: pt
Funcionalidade: Lista de Tarefas
    Como usuário, preciso inserir uma tarefa na minha
    lista de tarefas, para saber quais compromissos
    preciso cumprir.

    Cenario: Deve aceitar o cadastro de uma tarefa
        Dado que o usuário acesse a página da lista de tarefas
        Quando cadastrar a tarefa
        Então deve mostrar a mensagem de sucesso
        E a tarefa deve ser exibida na lista de tarefas