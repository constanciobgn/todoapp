from page_objects import PageObject, PageElement


class TodoPage(PageObject):
    title = PageElement(css='h1')
    alert = PageElement(css='.alert')
    input_name = PageElement(name='name')
    add_button = PageElement(css='input[type="submit"]')
    todo_list = PageElement(xpath='//table')

    def insert_task(self, task_name):
        self.input_name.send_keys(task_name)
        self.add_button.click()

    def total_tasks(self):
        return len(self.todo_list.find_elements_by_xpath('//tbody/tr'))
